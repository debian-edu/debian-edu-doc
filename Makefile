SRCDIR  := $(CURDIR)/documentation
DESTDIR =

all: build

update-sources-from-wiki:
	$(MAKE) -C $(SRCDIR)/debian-edu-bookworm update
	$(MAKE) -C $(SRCDIR)/debian-edu-bullseye update

update-copyright:
	$(MAKE) -C $(SRCDIR)/debian-edu-bookworm update-copyright
	$(MAKE) -C $(SRCDIR)/debian-edu-bullseye update-copyright
	$(MAKE) -C $(SRCDIR)/debian-edu-itil update-copyright
	$(MAKE) -C $(SRCDIR)/audacity update-copyright
	$(MAKE) -C $(SRCDIR)/rosegarden update-copyright
	cat debian/copyright.packaging \
		$(SRCDIR)/*/copyright.manual \
		debian/copyright.licence \
		> debian/copyright
	rm documentation/*/copyright.manual

readme status build install clean pdf epub:
	$(MAKE) -C $(SRCDIR)/debian-edu-bookworm $@
	$(MAKE) -C $(SRCDIR)/debian-edu-bullseye $@
	$(MAKE) -C $(SRCDIR)/rosegarden $@
	$(MAKE) -C $(SRCDIR)/audacity $@
	$(MAKE) -C $(SRCDIR)/debian-edu-itil $@
