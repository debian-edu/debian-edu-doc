#!/bin/bash

# Copyright 2009-2023 Holger Levsen (holger@layer-acht.org)
# 
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

#
# send reminders about the status of the package
# run via cron by holger on paradis.debian.org
#
#   holger@paradis:~$ crontab -l
#   # m h  dom mon dow   command
#   # run on the 1st sunday of a month
#   23 1 * * 0 if [ $(date +\%d) -le 7 ] ; then chronic /home/holger/debian-edu-doc/debian/mail_stats_to_list ; fi

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games"
set -x

report_problem(){
	echo "Problem running git $1 (return status $2) in paradis.debian.org:~holger/debian-edu-doc - please fix. Probably temporary though."
	exit 1
}

cd ~holger/debian-edu-doc
PULLED=false
for i in $(seq 1 10) ; do
	if $(git pull > /dev/null 2>&1) ; then
		PULLED=true
		break
	fi
	sleep $(( 30 * $i )) # sleep 30s, then 60s, ...
done
if ! $PULLED ; then
	report_problem pull unknown
fi

git status 2>&1
RESULT=$?
if [ $RESULT -ne 0 ] ; then
	report_problem status $RESULT
fi

#
# loop through some manuals
#
cd documentation
MANUALS="debian-edu-bookworm"
#MANUALS="debian-edu-trixie debian-edu-bookworm"
for i in $MANUALS ; do
	if [ $(ls $i/$i-manual.*.po > /dev/null 2>&1 ; echo $?) -eq 0 ] ; then
		cd $i 
		TMPFILE=$(mktemp)
		echo "The (translated) $i manual in PDF, ePUB or HTML formats are available at https://jenkins.debian.net/userContent/debian-edu-doc//" > $TMPFILE
		echo >> $TMPFILE
		echo "To understand this mail better, please read /usr/share/doc/debian-edu-doc/README." >> $TMPFILE
		echo "This mail is automatically send by a cronjob run by Holger Levsen every two weeks. Please send feedback, suggestions, flames and cookies via this list." >> $TMPFILE
		echo >> $TMPFILE
		make update > /dev/null 2>&1 
		make status >> $TMPFILE 2>&1 
		cat $TMPFILE | mail -s "Content and translation status for the $i manual" debian-edu@lists.debian.org
		rm $TMPFILE
		cd ..
	fi
done
cd ..

make clean
git reset --hard

