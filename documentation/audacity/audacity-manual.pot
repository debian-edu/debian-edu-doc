# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-10-06 00:00+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <article>
msgid "en"
msgstr ""

#. type: Content of: <article><articleinfo><title>
msgid "Debian Edu / Skolelinux Audacity manual"
msgstr ""

#. type: Content of: <article><articleinfo><subtitle><para>
msgid "Publish date:"
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Audacity manual"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"This is a manual for audacity, based on the 1.2.4b-2.1+b1 version from the "
"Debian Edu Etch 3.0 release."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"The version at <ulink url=\"http://wiki.debian.org/DebianEdu/Documentation/"
"Manuals/Audacity\"/> is a wiki and updated frequently."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"<link linkend=\"Translations\">Translations</link> are part of the "
"<computeroutput>debian-edu-doc</computeroutput> package, which can be "
"installed on a webserver, and is available <ulink url=\"http://maintainer."
"skolelinux.org/debian-edu-doc/\">online</ulink>."
msgstr ""

#. type: Content of: <article><section><title>
msgid "Before you start"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"Setting up Audacity is not hard, if you remember to setup Audacity to use "
"JACK, this is to make it possible to run other programs that use sound at "
"the same time. The guide for setting up JACK to run smoothly is here:"
msgstr ""

#. type: Content of: <article><section><para>
msgid "I used Audacity 1.3.3-Beta when i make this documentation."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"So to set Audacity to use jack you have to go in the menu --&gt; Edit ---"
"&gt; Preferences."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "edittojack.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"The next thing you have to be aware of that the project rate must be the "
"same sample rate you have set in jack."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "samplerate.png"
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Plugins"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid "There are some plugins you probably want:"
msgstr ""

#. type: Content of: <article><section><section><informaltable><tgroup><tbody><row><entry><para>
msgid "** audio Plugins **"
msgstr ""

#. type: Content of: <article><section><section><informaltable><tgroup><tbody><row><entry><para>
msgid "swh-plugins"
msgstr ""

#. type: Content of: <article><section><section><informaltable><tgroup><tbody><row><entry><para>
msgid "mcp-plugins"
msgstr ""

#. type: Content of: <article><section><section><informaltable><tgroup><tbody><row><entry><para>
msgid "terminatorx"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid "fixme: put on some audio plugin you have experience with"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid "Now you are ready to start recording."
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Where do i find audacity"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid ""
"Well you can simply just install it with aptitude, and you can also take a "
"look at the projects page here <ulink url=\"http://audacity.sourceforge.net/"
"\"/>"
msgstr ""

#. type: Content of: <article><section><title>
msgid "Doing some recording"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"First thing to remember when you do a recording, is that you are careful "
"that the record volume settings, not set to loud. It's better to have to low "
"setting rather to loud on this."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "recording.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"And when you have recorded what you want, you can normalize the recording so "
"the volume automatically set right volume on the record without any danger "
"of have part of the sound overdriven."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "normalize.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid "And after you have normalized the recorded segment, it looks like this:"
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "normalized.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"Now the volume on this recorded segment is corrected. This is a better way "
"to be sure that the record you have done not are overdriven, so have to Low "
"setting and afterwards let audacity fix the volume on it."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"Now we want to remove noise from the record, maybe some background hiss from "
"your microphone. So first part is to make a \"noise profile\" of what you "
"want to remove from your recorded segment."
msgstr ""

#. type: Content of: <article><section><para>
msgid "1. First zoom out so you more clearly can see the recording."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "zoom.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"2. Then mark what you want to make a profile of. Buy left click on mouse and "
"hold down on the segment, and drag. You mark the zone you need to get the "
"profile of."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "marking.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid "3. Then choose that you want to use Noise removal."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "noiseremoval.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid "4. Then Create \"noise profile\"."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "getnoiseprofile.png"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"5. Then you choose the whole record by clicking CTRL + A, and choose noise "
"removal again from menu, but this time press OK."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"Then if you want to add some reverb, other nice effects to your liking, you "
"just search the big menu that is available after you installed the plugins I "
"recommended."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"You have several way to export your finished result. You can export it as "
"WAV, AIFF MP3, OGG, FLAC, just choose export in the file menu."
msgstr ""

#. type: Content of: <article><section><title>
msgid "Importing Audio"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"With audacity you can import audio of the regular variety: Audio, MIDI, "
"Labels and Raw Data. You can directly import MP3 files into the project, and "
"mixing different song together in audacity."
msgstr ""

#. type: Content of: <article><section><para>
msgid "Let me give you example."
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"First my main song \"smurfesangen\", and then you can import for mixing, one "
"another song, just choose in the menu &gt; &gt; file &gt; &gt; Import Audio, "
"then the newly imported audio will come under your first audio, the "
"possibility to mixing here is huge."
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "mixingaudio.png"
msgstr ""

#. type: Content of: <article><section><title>
msgid "Tool section"
msgstr ""

#. type: Content of: <article><section><para><inlinemediaobject><textobject><phrase>
msgid "toolbox.png"
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Selection tool"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid ""
"To split the audio so you can move part of it you have to first with the "
"selection tool, choose where you want to cut it. And then with \"ctrl + I\" "
"split it. And then with time shift tool you can move the segment."
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Zooming tool"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid ""
"With the Zooming tool you can zoom into the audio segment and then "
"manipulate the audio with the drawing tool."
msgstr ""

#. type: Content of: <article><section><section><para><inlinemediaobject><textobject><phrase>
msgid "manipulating.png"
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Envelope Tool"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid ""
"With the envelope tool you can manipulate the volume by increasing or "
"decreasing the volume wherever you are on the segment."
msgstr ""

#. type: Content of: <article><section><section><para><inlinemediaobject><textobject><phrase>
msgid "manipulating1.png"
msgstr ""

#. type: Content of: <article><section><section><title>
msgid "Time Shifting Tool"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid "with this you can move your audio segments around."
msgstr ""

#. type: Content of: <article><section><title>
msgid "Copyright and authors"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"This document is written and copyrighted by Alf Tonny Bätz (2008, 2009), "
"Holger Levsen (2009) and is released under the GPL2 or any later version. "
"Enjoy!"
msgstr ""

#. type: Content of: <article><section><para>
msgid ""
"If you add content to it, <emphasis role=\"strong\">please only do so if you "
"are the author of it and plan to release it under the same conditions</"
"emphasis>! Then add your name here and release it under the \"GPL v2 or any "
"later version\" licence."
msgstr ""

#. type: Content of: <article><section><title>
msgid "Appendix A - The GNU Public License"
msgstr ""

#. type: Content of: <article><section><section><para>
msgid ""
"Copyright (C) 2008,2009 Alf Tonny Bätz &lt; <ulink url=\"mailto:alfton@gmail."
"com\">alfton@gmail.com</ulink> &gt; and others, see the <link linkend="
"\"CopyRight\">Copyright chapter</link> for the full list of copyright owners."
msgstr ""
