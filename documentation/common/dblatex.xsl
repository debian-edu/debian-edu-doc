<?xml version='1.0' encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <xsl:param name="latex.encoding">utf8</xsl:param>
  <xsl:param name="latex.class.options">10pt,onecolumn</xsl:param>
  <xsl:param name="xetex.font">
   <xsl:choose>
    <xsl:when test="contains(/article/@lang,'ja')">
	<xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
	<xsl:text>\setCJKmainfont{IPAPGothic}&#10;</xsl:text>
	<xsl:text>\setCJKsansfont{IPAPGothic}&#10;</xsl:text>
	<xsl:text>\setCJKmonofont{IPAexGothic}&#10;</xsl:text>
        <xsl:text>\setmainfont{Latin Modern Sans}&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="contains(/article/@lang,'zh-cn')">
	<xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
	<xsl:text>\setCJKmainfont{WenQuanYi Micro Hei}&#10;</xsl:text>
	<xsl:text>\setCJKsansfont{WenQuanYi Micro Hei}&#10;</xsl:text>
	<xsl:text>\setCJKmonofont{WenQuanYi Micro Hei Mono}&#10;</xsl:text>
        <xsl:text>\setmainfont{Latin Modern Sans}&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="contains(/article/@lang,'zh-tw')">
	<xsl:text>\usepackage{xeCJK}&#10;</xsl:text>
	<xsl:text>\setCJKmainfont{WenQuanYi Micro Hei}&#10;</xsl:text>
	<xsl:text>\setCJKsansfont{WenQuanYi Micro Hei}&#10;</xsl:text>
	<xsl:text>\setCJKmonofont{WenQuanYi Micro Hei Mono}&#10;</xsl:text>
        <xsl:text>\setmainfont{Latin Modern Sans}&#10;</xsl:text>
    </xsl:when>
    <xsl:when test="contains(/article/@lang,'uk')">
	<xsl:text>\usepackage[ukrainian]{babel}&#10;</xsl:text>
        <xsl:text>\setmainfont{DejaVuSans}&#10;</xsl:text>
        <xsl:text>\setsansfont{DejaVuSans}&#10;</xsl:text>
        <xsl:text>\setmonofont{DejaVuSansMono}&#10;</xsl:text>
    </xsl:when>
    <xsl:otherwise>
        <xsl:text>\setmainfont{Latin Modern Sans}&#10;</xsl:text>
        <xsl:text>\setsansfont{Latin Modern Sans}&#10;</xsl:text>
        <xsl:text>\setmonofont{Latin Modern Mono Light}&#10;</xsl:text>
    </xsl:otherwise>
   </xsl:choose>
  </xsl:param>
  <xsl:param name="toc.section.depth">3</xsl:param>
  <xsl:param name="doc.collab.show">0</xsl:param>
  <xsl:param name="latex.output.revhistory">0</xsl:param>
</xsl:stylesheet>
