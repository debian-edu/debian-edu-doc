README for translators of the debian-edu-itil-manual
--------------------------------------------


Before you start:
-----------------

Basic information about Salsa (the host where our Git repository is located)
and Git is available at https://wiki.debian.org/Salsa.

To commit your translations you need to be a member of the Salsa project
'Debian Edu'.

Then check out the debian-edu-doc source using ssh access:

$ git clone git@salsa.debian.org:debian-edu/debian-edu-doc.git

If you only want to translate, you need to check out only a few files from Git
(which can be done anonymously). Please file a bug against the debian-edu-doc
package and attach the compressed PO file to the bugreport, for already existing
bug reports,see:

https://bugs.debian.org/debian-edu-doc

For instructions on how to submit bugs, see:

https://www.debian.org/Bugs/

You can check out the debian-edu-doc source anonymously with the following
command (you need to have the git package installed for this to work):

$ git clone https://salsa.debian.org/debian-edu/debian-edu-doc.git

Then edit the file documentation/debian-edu-itil-manual.$CC.po (replacing $CC with your
language code). There are many tools for translating available, e.g. Lokalize
or Poedit.

Then you either commit the file directly to Git (if you have the rights to do
so) or add the file to the bugreport.

To update your local copy of the repository use the following command inside
the debian-edu-doc directory:

$ git pull

Install the build dependencies of the package plus some extra packages (which
are needed for working on the translations, the package itself builds fine
without them):

sudo apt build-dep debian-edu-doc
sudo apt install po4a texlive-latex-recommended texlive-latex-extra \
texlive-fonts-recommended texlive-fonts-extra dblatex wget libxml2-utils

Please note: pdflatex depends on ($texlive-packages | $tetex-packages) - we
need the texlive packages.

For the following commands it's assumed you're in the current directory:
debian-edu-doc/documentation/debian-edu-itil



How to start a translation
--------------------------

Simply copy the POT file as a PO file:

$ cp debian-edu-itil-manual.pot debian-edu-itil-manual.da.po	# create a new Danish PO file
$ git add debian-edu-itil-manual.da.po				# add it to the VCS
$ vi debian-edu-itil-manual.da.po					# better use Lokalize or Poedit
$ LINGUA=da make xml							# check the build and rewrap the PO file
$ git commit debian-edu-itil-manual.da.po				# commit the new translation for the first time
$ git push								# update the remote repository


How to update the translations
------------------------------

Update the translations every time before you start translating some new
strings:

$ git pull					# update from the VCS, maybe someone else translated
						# something or the English source was updated
$ vi debian-edu-itil-manual.da.po		# again, better use Lokalize or Poedit
$ LINGUA=da make xml				# check the build and rewrap the PO file
$ git diff debian-edu-itil-manual.da.po	# check the differences from the previous file
$ git commit debian-edu-itil-manual.da.po	# commit changes to your local repository
$ git push					# update the remote repository



How to build the HTML, PDF and EPUB versions.
---------------------------------------------

$ make html pdf epub		# build all versions for all translations.

Even faster for a single language, e.g. Danish:
$ LINGUA=da make html		# build only the Danish translation in HTML.

You can also build the PDF:
$ LINGUA=da make pdf		# build only the Danish translation in PDF.
The English pictures will be used for this test build, but localized images
will be used during the actual build process (if they exist).

Build the EPUB, too:
$ LINGUA=da make epub		# build only the Danish translation in PDF.
Existing localized images will replace English ones.

Cleaning up after having checked that things are okay:
$ make clean

Translating notes
-----------------

Don't translate URLs - be careful: some wiki-URLs might be harder to notice
they are URLs.

To "translate" images, add them to the VCS into the images/$CC/ directory, where
$CC is your language. They will be picked up automatically.

If you spot errors in the English version or see how it could be improved,
please do contribute to the English version too. Just edit the wiki at
https://wiki.debian.org/DebianEdu/Documentation/ITIL/ - a user account 
is needed to edit these pages, so you will probably need to create a wiki
user first (via the login link).

If you cannot commit your work to the VCS directly, please file a bug against
the debian-edu-doc package, see https://bugs.debian.org/debian-edu-doc. Compress
the PO file using gzip and attach the file to the bugreport. Instructions how
to submit bugs are available at https://bugs.debian.org


-- Holger Levsen, holger@debian.org, last updated: 2020-08-23
